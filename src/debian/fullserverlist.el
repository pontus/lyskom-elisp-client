(def-kom-var kom-builtin-server-aliases
  '(("kom.lysator.liu.se" . "LysKOM")
    ("com.lysator.liu.se" . "LysCOM (LysKOM in English)")
    ("kom.ludd.ltu.se" . "LuddKOM")
    ("kom.hem.liu.se" . "RydKOM")
    ("kom.update.uu.se" . "UppKOM")
    ("kom.mds.mdh.se" . "MdS-KOM")
    ("kom.stacken.kth.se" . "TokKOM")
    ("com.helsinki.fi" . "HesaKOM")
    ("kom.cd.chalmers.se" . "CD-KOM")
    ("community.roxen.com" . "Roxen Community KOM")
    ("kom.ds.hj.se" . "DSKOM")
    ("kom.sno.pp.se" . "SnoppKOM")
    ("myskom.kfib.org" . "MysKOM"))
  "**An alist mapping server names to shorter identification strings.

Each value in this string should be of the form (`SERVER' . `NICKNAME'),
where `NICKNAME' is the short name for the server `SERVER'. Avoid
setting this variable since that will override the list compiled into
the client. Use `kom-server-aliases' instead.

Values other than those described are reserved for future use.")
